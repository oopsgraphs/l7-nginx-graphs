# L7-nginx-graphs

Files to create an layer7 online(web) graph , with highcharts and nginx.

Like this ![Graph demo](https://screen.mathias-basire.me/2019-02-11_00-36-12.jpg)

## Install:
- for ubuntu/debian : 
``` 
wget https://gitlab.com/oopsgraphs/l7-nginx-graphs/raw/master/auto_install.sh && sudo chmod +x auto_install.sh
./auto_install
```

- for centos/fedora :
```
soon :/
```

#### ⚠️️This is for educational purpose only.⚠️
