apt-get install sudo curl -y
sudo apt-get update -y && sudo apt-get upgrade -y
sudo apt-get install nginx -y
sudo mkdir /var/www/default
sudo wget "https://cdn.vectahosting.eu/403.html" -O /var/www/default/403.html
sudo wget "https://cdn.vectahosting.eu/404.html" -O /var/www/default/404.html
sudo rm /etc/nginx/sites-enabled/default
sudo wget "https://gitlab.com/oopsgraphs/l7-nginx-graphs/raw/master/nginx/default.conf" -O /etc/nginx/sites-enabled/default.conf
sudo wget "https://gitlab.com/oopsgraphs/l7-nginx-graphs/raw/master/graph/l7.html" -O /var/www/default/l7.html
sudo nginx -t
sudo service nginx restart
echo "Install : OK"
echo "This url contains graphs"
echo ""
IP=$(curl -s https://api.vectahosting.eu/ip)
echo "http://$IP/l7.html"
